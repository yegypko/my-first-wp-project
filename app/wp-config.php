<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_my_first_wp_project');

/** MySQL database username */
define('DB_USER', 'master_firstProject');

/** MySQL database password */
define('DB_PASSWORD', 'YnKVx82gfWbli86y');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZU_*J@[ndVjDj)1m0f}1AWkeHZ|P%Kay(PzGTXOJpby)t:<:7#Jf_nT|-.IrR;o;');
define('SECURE_AUTH_KEY',  'XT+Shm9TZB&<1`Hxi`78WHo-%aDRs*+U/diYvxP|9v.GJ#xY9 >SO4)@nYkp|c;s');
define('LOGGED_IN_KEY',    '`u@}o c* oTQ8`_YA*Y.8RWx eA^;g3}<wU2<zk5BvpfIP|H!MCVB)GIlOmeNY1c');
define('NONCE_KEY',        ')-+|xn2+T[8Q|3MPfl4JJ[na!;Hz_=@Vpk@d4t F*sVmJ3PT2qdQHoGh_967<)[9');
define('AUTH_SALT',        'j@B$v$Mg!<$s{f%v~m~foSOQLbu-qJ` b)[La%]=Atz]YUK1HtY-Prk8Sz^V2D]-');
define('SECURE_AUTH_SALT', 'OQ}l7X^<d9Ayq=tI+h1Q* Fy9S?F[KB<T5o=m{I2J/%qKEMMio_/Bq&}pTEY|!X|');
define('LOGGED_IN_SALT',   'OL#vd*P&C>ZT(TVWX`~jGC.N WJ9e,glu#4w.XHS7k|0t]|NTEhV&.`+P,m0n8!4');
define('NONCE_SALT',       'eTOxB2;U8uo9j8&_o!-fTjmJ!VlZz}eiR@b(U8|A5GUl7?BC9g=o#FkKW`:Z.wH7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');